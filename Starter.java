package main;
/*
1 Создайте массив из 8 случайных целых чисел из отрезка [1;10]
2 Выведите массив на экран в строку
3 Далее определите и выведите на экран сообщение о том, является ли массив строго возрастающей последовательностью
4 Замените каждый элемент с нечётным индексом на ноль
5 Снова выведете массив на экран на отдельной строке
 */
public class Starter {
    public static void main(String[] args) {
        //Дано
        final int min = 1;
        int max = 10;
        int weight = max - min + 1; // Прибавление 1 нужно, тк у нас диапазон включает 10
        final int arrayLength = 8;
        int[] randomNumbers = new int[arrayLength];
        boolean increase = true;
        // №1
        // random() дает число в диапазоне [0, 1), умножаем на вес, приводим к int => переходим в диапазон [0 9],
        // чтоб попасть в [1,10] прибавляем min
        for (int i = 0; i < arrayLength; i++) {
            randomNumbers[i] = (int) (Math.random() * weight) + min;
        }

        // №2
        for (int i:randomNumbers) {
            System.out.print(i +" ");
        }

        // №3
        for (int i = 0; i < arrayLength-1; i++) {
            if (randomNumbers[i]>=randomNumbers[i+1]){
                System.out.println("\nМассив не является строго возрастающей последовательностью!");
                increase = false;
                break;
            }
        }
        if (increase) System.out.println("\nМассив является строго возрастающей последовательностью!");

        // №4
        for (int i = 0; i < arrayLength-1; i+=2) {
                randomNumbers[i] = 0;
        }

        // 5
        for (int i:randomNumbers) {
            System.out.print(i +" ");
        }
    }
}
